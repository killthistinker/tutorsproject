﻿using Ardalis.ApiEndpoints;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Responses.AccountModels;
using Responses.AccountModels.Responses;
using Responses.DataTransferLib;
using User.Infrastructure.Contracts.Services;
using StatusCodes = Responses.Enums.StatusCodes;


namespace User.API.Endpoints.Account;
[ApiController]
    [Route("[controller]")]
    public class AuthenticateEndpoint : EndpointBaseAsync.WithRequest<UserAuthDto>.WithActionResult<DefaultResponseObject<AuthenticateResponse>>
    {
        private readonly IAccountService _accountService;
        private readonly IMapper _mapper;
        private readonly ILogger<AuthenticateEndpoint> _logger;

        public AuthenticateEndpoint(IAccountService accountService, IMapper mapper, ILogger<AuthenticateEndpoint> logger)
        {
            _accountService = accountService;
            _mapper = mapper;
            _logger = logger;
        }

        
        [HttpPost("/Account/Login")]
        public override async Task<ActionResult<DefaultResponseObject<AuthenticateResponse>>> HandleAsync(UserAuthDto request, CancellationToken cancellationToken = new CancellationToken())
        {
            try
            {
                var response = await _accountService.Authenticate(request);
                if(!response.IsSuccess)  return Ok(response);

                return (_mapper.Map<DefaultResponseObject<AuthenticateResponse>>(response));
            }
            catch (NullReferenceException e)
            {
                _logger.LogInformation(e, "Пользователь не найден");
                return Ok(new DefaultResponseObject<AuthenticateResponse>
                {
                    ErrorModel = new ErrorModel{Message = "Неверное имя пользователя или пароль", StatusCode = StatusCodes.UserNotFound}
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Произошла ошибка");
                return Ok(StatusCodes.ServiceIsUnavailable);
            } 
        }
}