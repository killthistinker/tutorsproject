﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using User.Infrastructure.Contracts.Services;

namespace User.API.Endpoints.Room;

[ApiController]
[Route("[controller]")]
public class GetAllRooms : EndpointBaseSync.WithoutRequest.WithActionResult<DefaultResponseObject<IEnumerable<RoomDto>>>
{
    private readonly IRoomService _roomService;

    public GetAllRooms(IRoomService roomService)
    {
        _roomService = roomService;
    }
    
    [HttpGet("/Room/All")]
    public override ActionResult<DefaultResponseObject<IEnumerable<RoomDto>>> Handle()
    {
        var rooms = _roomService.GetAll();
    
        return Ok(rooms);
    }
}