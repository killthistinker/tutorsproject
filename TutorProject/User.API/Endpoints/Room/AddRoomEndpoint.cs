﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using User.Infrastructure.Contracts.Services;
using StatusCodes = Responses.Enums.StatusCodes;

namespace User.API.Endpoints.Room;

[ApiController]
[Route("[controller]")]
public class AddRoomEndpoint : EndpointBaseAsync.WithRequest<RoomModelRequest>.WithActionResult<DefaultResponseObject<StatusCodes>>
{
    private readonly IRoomService _roomService;

    public AddRoomEndpoint(IRoomService profileService)
    {
        _roomService = profileService;
    }
    
    [HttpPost("/Room/Add")]
    public override async Task<ActionResult<DefaultResponseObject<StatusCodes>>> HandleAsync(RoomModelRequest request, CancellationToken cancellationToken = new CancellationToken())
    {
        var response = await _roomService.Add(request);

        return Ok(response);
    }
}