
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/Main/IndexPage.vue') },
      { path: '/rooms', component: () => import('pages/Room/AllRooms') },
      { path: '/room/:name/:address', component: () => import('pages/Room/RoomPage') }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/profile', component: () => import('pages/Profile/ProfilePage.vue') },
      { path: '/lesson', component: () => import('pages/Profile/LessonPage.vue') },
      { path: '/profile/addTeacher', component: () => import('pages/Profile/AddTeacherPage') },
      { path: '/profile/schedule', component: () => import('pages/Profile/Schedule') }
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  },
  {
    path: '/',
    component: () => import('layouts/EmptyLayout'),
    children: [
      { path: '/registration', component: () => import('pages/Account/RegistrationPage.vue') },
      { path: '/login', component: () => import('pages/Account/LoginPage.vue') }
    ]
  }
]

export default routes
