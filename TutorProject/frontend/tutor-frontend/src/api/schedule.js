import api from '../requests'

export const getEmptyPeriod = async (payload) => await api.get('/GetEmptyPeriod', payload).catch(error => { console.log(error) })
