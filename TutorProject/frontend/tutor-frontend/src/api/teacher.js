import api from '../requests'

api.defaults.headers.common['Content-Type'] = 'multipart/form-data'
export const addCertificate = async (payload) => await api.post('/Tutor/Add', payload).catch(error => { console.log(error) })

export const getAllSubjects = async () => await api.get('/Subjects/All').catch(error => { console.log(error) })
