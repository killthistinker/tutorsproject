﻿using System.Text.Json.Serialization;

namespace Schedule.Domain.Entities;

public class AvailableHour
{
    [JsonPropertyName("month")]
    public int Month { get; set; }
    
    [JsonPropertyName("day")]
    public int Day { get; set; }
    
    [JsonPropertyName("hours")]
    public List<Hour>? Hours { get; set; }
}