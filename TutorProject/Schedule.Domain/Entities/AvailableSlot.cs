﻿namespace Schedule.Domain.Entities;

public class AvailableSlot
{
    public List<AvailableHour>? Hours { get; set; }
}