﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Schedule.Domain.Entities;

public class Period
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public long UserId { get; set; }
    public AvailableSlot AvailebleSlots { get; set; }
}