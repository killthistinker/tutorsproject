﻿namespace Tutor.Domain.Models.Dto;

public class TutorDto
{
    public string Email { get; set; }
    public IEnumerable<SubjectDto> Subjects { get; set; }
}