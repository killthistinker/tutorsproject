﻿namespace Tutor.Domain.Models.Dto;

public class SubjectDto
{
    public string Name { get; set; }
}