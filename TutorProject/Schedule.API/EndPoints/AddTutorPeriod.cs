﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using Responses.DataTransferLib.ScheduleDTO;
using Schedule.Infrastructure.Contracts.Services;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Schedule.API.EndPoints;

[ApiController]
[Route("[controller]")]

public class AddTutorPeriod : EndpointBaseAsync.WithRequest<SelectedHoursDto>.WithActionResult<DefaultResponseObject<StatusCodes>>
{
    private readonly IPeriodService _periodService;

    public AddTutorPeriod(IPeriodService periodService)
    {
        _periodService = periodService;
    }
    
    [HttpPost("AddAvailablePeriod")]
    public override async Task<ActionResult<DefaultResponseObject<StatusCodes>>> HandleAsync(SelectedHoursDto request, CancellationToken cancellationToken = new CancellationToken())
    {
        try
        {
            var response = await _periodService.AddAvailablePeriod(request);
            return Ok(response);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }
}