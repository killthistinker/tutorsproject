﻿using Payment.Domain;
using Payment.Interfaces;

namespace Payment.Infrastructure.Worker
{
    public class CheckBackgroundWorker
    {
        private readonly IPayboxIntegration _paybox;
        private readonly IPaymentRepository _repository;
        public CheckBackgroundWorker(IPayboxIntegration paybox, IPaymentRepository repository)
        {
            _paybox = paybox;
            _repository = repository;
        }

        public async Task CheckStatusPayment()
        {
            try
            {
                var payments = _repository.GetAll().Where(s => s.Status == "pending" || s.Status =="partial" && s.CheckCount < 5);
                if (payments != null)
                {
                    foreach (var payment in payments)
                    {
                        var result = await _paybox.GetStatusPayment(new PaymentStatus() { PayboxId = payment.PayboxId, OrderId = payment.Id.ToString() });
                        payment.Status = result.Value.TransactionStatus;
                        payment.Card = result.Value.Card;
                        payment.CheckCount++;
                        _repository.UpdateLog(payment);
                        _repository.Save();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
