using Microsoft.EntityFrameworkCore;
using Payment.Domain.Entities;
using Payment.Infrastructure.Data.DbMap;

namespace Payment.Infrastructure.Data;

public class TutorDbContext : DbContext
{
    public DbSet<TransactionLog> Logs { get; set; }

    public TutorDbContext(DbContextOptions<TutorDbContext> options) : base(options)
    {
        AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
    }
    
    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.ApplyConfiguration(new TransactionLogDbMap());
    }
}