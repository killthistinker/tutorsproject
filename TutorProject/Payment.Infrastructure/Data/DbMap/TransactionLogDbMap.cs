﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Payment.Domain.Entities;

namespace Payment.Infrastructure.Data.DbMap;

public class TransactionLogDbMap : IEntityTypeConfiguration<TransactionLog>
{
    public void Configure(EntityTypeBuilder<TransactionLog> builder)
    {
        builder.Property(p => p.Email).HasColumnType("VARCHAR(100)").IsRequired();
        builder.Property(p => p.LessonName).HasColumnType("VARCHAR(100)").IsRequired();
        builder.Property(p => p.Status).HasColumnType("VARCHAR(100)").IsRequired();
        builder.Property(p => p.Card).HasColumnType("VARCHAR(100)").IsRequired();
        builder.Property(p => p.Amount).HasColumnType("INTEGER").IsRequired();
        builder.Property(p => p.PayboxId).HasColumnType("INTEGER").IsRequired();
        builder.Property(p => p.CheckCount).HasColumnType("INTEGER").IsRequired();
        builder.Property(p => p.LessonDate).HasColumnType("timestamp without time zone").IsRequired();
        builder.Property(p => p.CreateDate).HasColumnType("timestamp without time zone").IsRequired();
    }
}