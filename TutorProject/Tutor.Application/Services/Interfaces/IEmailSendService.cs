﻿using MimeKit;
using Responses.DataTransferLib.TutorDtos;

namespace Tutor.Application.Services.Interfaces
{
    public interface IEmailSendService
    {
        Task SendEmailTutorNotification(TutorCreateDto user); 
        Task SendEmailAsync(MimeMessage emailMessage);
        Task SendRefuseEmail(RefuseTutorDto model);
    }
}