﻿using MongoDB.Driver;
using Schedule.Domain.Entities;

namespace Schedule.Infrastructure.Data;

public class ScheduleContextSeed
{
    public static void SeedData(IMongoCollection<Hour> hours)
    {
        bool existHour = hours.Find(p => true).Any();
        if (!existHour)
        {
            hours.InsertMany(GetPreconfiguredList());
        }
    }
    
    private static IEnumerable<Hour> GetPreconfiguredList()
    {
        return new List<Hour>()
        {
            new() { HourName = "00:00:00" },
            new() { HourName = "01:00:00" },
            new() { HourName = "02:00:00" },
            new() { HourName = "03:00:00" },
            new() { HourName = "04:00:00" },
            new() { HourName = "05:00:00" },
            new() { HourName = "06:00:00" },
            new() { HourName = "07:00:00" },
            new() { HourName = "08:00:00" },
            new() { HourName = "09:00:00" },
            new() { HourName = "10:00:00" },
            new() { HourName = "11:00:00" },
            new() { HourName = "12:00:00" },
            new() { HourName = "13:00:00" },
            new() { HourName = "14:00:00" },
            new() { HourName = "15:00:00" },
            new() { HourName = "16:00:00" },
            new() { HourName = "17:00:00" },
            new() { HourName = "18:00:00" },
            new() { HourName = "19:00:00" },
            new() { HourName = "20:00:00" },
            new() { HourName = "21:00:00" },
            new() { HourName = "22:00:00" },
            new() { HourName = "23:00:00" }
        };
    }
}