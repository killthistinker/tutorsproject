﻿using MongoDB.Driver;
using Schedule.Domain.Entities;

namespace Schedule.Infrastructure.Data;

public interface IScheduleContext
{
    IMongoCollection<Period> Periods { get; }
    IMongoCollection<Hour> Hours { get; }
}