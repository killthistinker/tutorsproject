﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Schedule.Infrastructure.Contracts.Repository;
using Schedule.Infrastructure.Contracts.Repository.Interfaces;
using Schedule.Infrastructure.Data;

namespace Schedule.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructureServices(this IServiceCollection services)
    {
        services.AddSingleton<IScheduleContext, ScheduleContext>();
        services.AddScoped<IPeriodRepository, PeriodRepository>();
        return services;
    }

}