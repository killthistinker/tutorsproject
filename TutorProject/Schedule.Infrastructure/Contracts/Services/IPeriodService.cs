﻿using Responses.DataTransferLib;
using Responses.DataTransferLib.ScheduleDTO;
using Schedule.Domain.Entities;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Schedule.Infrastructure.Contracts.Services;

public interface IPeriodService 
{
    public Task<DefaultResponseObject<StatusCodes>> AddAvailablePeriod(SelectedHoursDto selectedHoursDto);
    public DefaultResponseObject<EmptyPeriodDto> GetEmptyPeriod();
}