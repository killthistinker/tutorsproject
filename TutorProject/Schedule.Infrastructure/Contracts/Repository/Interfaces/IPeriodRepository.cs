﻿using Schedule.Domain.Entities;

namespace Schedule.Infrastructure.Contracts.Repository.Interfaces;

public interface IPeriodRepository
{
    public void Create(Period period);
    public List<Hour> GetTime();
}