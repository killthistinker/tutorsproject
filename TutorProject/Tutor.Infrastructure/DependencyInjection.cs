﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Tutor.Infrastructure.Contracts.Repository;
using Tutor.Infrastructure.Contracts.Repository.Interfaces;
using Tutor.Infrastructure.Data.TutorDbContext;

namespace Tutor.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
    {
        var connection = configuration.GetConnectionString("DefaultConnection");
        services.AddDbContext<TutorDbContext>(ops => ops.UseNpgsql(connection));
        services.AddScoped<ITutorRepository, TutorRepository>();
        return services;
    }
}