﻿using Microsoft.EntityFrameworkCore;
using Tutor.Infrastructure.Data.DbMap;


namespace Tutor.Infrastructure.Data.TutorDbContext;

public class TutorDbContext : DbContext
{
    public DbSet<Domain.Entities.Tutor> Tutors { get; set; }
    
    public TutorDbContext(DbContextOptions<TutorDbContext> options) : base(options)
    {
        Database.Migrate();
    }
    
    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.ApplyConfiguration(new SpecialiseDbMap());
        builder.ApplyConfiguration(new TutorDbMap());
    }
}