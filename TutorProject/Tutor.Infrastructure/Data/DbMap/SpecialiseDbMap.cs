﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tutor.Domain.Entities;

namespace Tutor.Infrastructure.Data.DbMap;

public class SpecialiseDbMap : IEntityTypeConfiguration<Domain.Entities.Specialise>
{
    public void Configure(EntityTypeBuilder<Domain.Entities.Specialise> builder)
    {
        builder.Property(p => p.Name).HasColumnType("VARCHAR(50)").IsRequired();
        builder.HasIndex(p => p.Name).IsUnique();
        builder.Property(p => p.SubjectName).HasColumnType("VARCHAR(50)").IsRequired();
        builder.HasIndex(p => p.SubjectName).IsUnique();
    }
}