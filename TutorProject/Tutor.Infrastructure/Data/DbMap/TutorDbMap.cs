﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Tutor.Infrastructure.Data.DbMap;

public class TutorDbMap : IEntityTypeConfiguration<Domain.Entities.Tutor>
{
    public void Configure(EntityTypeBuilder<Domain.Entities.Tutor> builder)
    {
        builder.Property(p => p.Email).HasColumnType("VARCHAR(100)").IsRequired();
        builder.HasMany(x => x.Specialises).WithOne(x => x.Tutor).HasForeignKey(x => x.TutorId);
    }
}