using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Payment.Domain;
using Payment.Domain.Entities;
using Payment.Infrastructure;
using Payment.Interfaces;
using Responses.DataTransferLib;
using Responses.Payments;

namespace Payment.API.Endpoints.Paybox;
[ApiController]
[Route("[controller]")]
public class GetTransactionsEndpoint: EndpointBaseSync.WithRequest<GetPaymentDto>.WithActionResult<DefaultResponseObject<List<Transaction>>>
{
    private readonly IPaymentRepository _repository;
    public GetTransactionsEndpoint(IPaymentRepository repository)
    {
        _repository = repository;
    }
    [HttpPost("/Payment/GetPayments")]
    public override ActionResult<DefaultResponseObject<List<Transaction>>> Handle(GetPaymentDto email)
    {
        var result = _repository.GetAll().Where(w => w.Email == email.Email).ToList();
        if (result.Any())
        {
            var transactions = new List<Transaction>();
            transactions.AddRange(result.Select(s=> new Transaction()
            {
                Email = s.Email,
                Amount = s.Amount,
                Status = s.Status,
                Card = s.Card,
                CreateDate = s.CreateDate,
                PayboxId = s.PayboxId,
                LessonName = s.LessonName
            }));
            return Ok(new DefaultResponseObject<List<Transaction>>()
            {
                IsSuccess = true,
                Value = transactions
            });
        }
        
        return Ok(new DefaultResponseObject<List<Transaction>>{IsSuccess = false});
    }
}
