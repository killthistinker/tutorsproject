using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Payment.Domain;
using Payment.Domain.Entities;
using Payment.Infrastructure;
using Payment.Interfaces;
using Responses.AccountModels;

namespace Payment.API.Endpoints.Paybox;

[ApiController]
[Route("[controller]")]
public class GetStatusPayment : EndpointBaseAsync.WithRequest<int>.WithActionResult<int>
{
    private readonly IPayboxIntegration _paybox;
    private readonly IPaymentRepository _repository;
    public GetStatusPayment(IPayboxIntegration paybox, IPaymentRepository repository)
    {
        _paybox = paybox;
        _repository = repository;
    }
    [HttpPost("/Payment/GetStatus")]
    public override async Task<ActionResult<int>> HandleAsync(int payboxId, CancellationToken cancellationToken = new CancellationToken())
    {
        var order = _repository.GetAll().FirstOrDefault(w=>w.PayboxId == payboxId);
        var data = new PaymentStatus() {PayboxId = payboxId,OrderId = order.Id.ToString()};
        var result  =_paybox.GetStatusPayment(data);
        if (result.Result.IsSuccess)
            order.Card = result.Result.Value.Card;
        order.Status = result.Result.Value.TransactionStatus;
        _repository.UpdateLog(order);
        _repository.Save();
        return Ok(result.Result.Value);
    }
}