using Payment.Domain;


namespace Payment.API;

public static class DependencyInjection
{
    public static IServiceCollection AddPayboxConfig(this IServiceCollection services, IConfiguration configuration)
    {
        PayboxConfig payboxConfig = configuration.GetSection("PayboxConfig").Get<PayboxConfig>();
        services.AddSingleton(payboxConfig);
        services.AddCors(options => options.AddPolicy("CorsPolicy", policy =>
        {
            policy.WithOrigins("https://localhost:7063").AllowAnyMethod().AllowAnyHeader().AllowCredentials();
        }));
        return services;
    }
}