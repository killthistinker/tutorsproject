﻿using AutoMapper;
using Responses.DataTransferLib;
using Responses.DataTransferLib.ScheduleDTO;
using Schedule.Domain.Entities;
using Schedule.Infrastructure.Contracts.Repository.Interfaces;
using Schedule.Infrastructure.Contracts.Services;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Schedule.Application.Services;

public class PeriodService : IPeriodService
{
    private readonly IPeriodRepository _periodRepository;
    private readonly IMapper _mapper;

    public PeriodService(IPeriodRepository periodRepository, IMapper mapper)
    {
        _periodRepository = periodRepository;
        _mapper = mapper;
    }
    public async Task<DefaultResponseObject<StatusCodes>> AddAvailablePeriod(SelectedHoursDto selectedHoursDto)
    { 
        List<SelectedHoursDto> availableHour = new List<SelectedHoursDto>();
        for (int i = 0; i < selectedHoursDto.HourStrings.Length; i++)
        {
            var hour = selectedHoursDto.HourStrings[i];
        }
                
        Period period = new Period()
        {
            UserId = 123456,
            // AvailebleSlots = data
        };
        return new DefaultResponseObject<StatusCodes> { Value = StatusCodes.ServiceOk };
    }
    public DefaultResponseObject<EmptyPeriodDto> GetEmptyPeriod()
    {
        try
        {
            var hours = _periodRepository.GetTime();
            var hoursDto = hours.Select(hour => _mapper.Map<HourDto>(hour)).ToList();
            List<AvailableHourDto> emptySlots = new List<AvailableHourDto>();
            int day = DateTime.Now.Day;
            int month = DateTime.Today.Month;
            for (int i = 0; i < 14; i++)
            {
                if (DateTime.DaysInMonth(DateTime.Today.Year, month) < i + DateTime.Now.Day)
                {
                    if (day >= 1 && day < DateTime.DaysInMonth(DateTime.Today.Year, month))
                        day += 1;
                    else
                    {
                        day = 1;
                        month += 1;
                    }
                    if (month + 1 > 12 && month != 12)
                        month = 1;
                }
                if (month == DateTime.Now.Month)
                    day = i + DateTime.Now.Day;
            
                emptySlots.Add(new AvailableHourDto()
                {
                    Day = day,
                    Month = month,
                    Hours = hoursDto
                });
            }

            AvailableSlotDto slots = new AvailableSlotDto()
            {
                Hours = emptySlots
            };

            EmptyPeriodDto periodDto = new EmptyPeriodDto()
            {
                AvailebleSlots = slots
            };

            return new DefaultResponseObject<EmptyPeriodDto?>{Value = periodDto};
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }
}