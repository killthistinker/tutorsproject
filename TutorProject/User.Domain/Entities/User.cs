﻿using Microsoft.AspNetCore.Identity;

namespace User.Domain.Entities
{
    public class User : IdentityUser<long>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RegisterDate { get; set; }
        public string DateOfBirth { get; set; }
        public bool IsActive { get; set; }
        public decimal Balance { get; set; }
    }
}
