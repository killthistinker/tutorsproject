﻿namespace User.Domain.Entities;

public class Room
{
    public long Id { get; set; }
    public string Name { get; set; }
    public string DateCreate { get; set; }
    public User Creator { get; set; }
    public long CreatorId { get; set; }
    public string Address { get; set; }
}