﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.AccountModels;
using Responses.DataTransferLib;
using Web.Gateway.Interfaces.Contracts;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Web.Gateway.API.Endpoints.User
{
    public class UserProfileEditEndpoint : EndpointBaseAsync.WithRequest<EditUserDto>.WithActionResult<DefaultResponseObject<DefaultResponse>>
    {
        private readonly IUserService _userService;
        private readonly ILogger<UserProfileEditEndpoint> _logger;

        public UserProfileEditEndpoint(IUserService userService, ILogger<UserProfileEditEndpoint> logger)
        {
            _userService = userService;
            _logger = logger;
        }

        [HttpPost("/Account/EditProfile")]
        public override async Task<ActionResult<DefaultResponseObject<DefaultResponse>>> HandleAsync(EditUserDto request, CancellationToken cancellationToken = new CancellationToken())
        {
            try
            {
                var email = HttpContext.Session.GetString("user");
                request.Email = email;

                var response = await _userService.EditAsync(request, cancellationToken);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Произошла ошибка");
                return Ok(StatusCodes.ServiceIsUnavailable);
            }
            
        }
    }
}
