using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Web.Gateway.API.Endpoints.User.Account;

[ApiController]
[Route("[controller]")]
public class GetUserDataEndpoint : EndpointBaseSync.WithoutRequest.WithActionResult
{

    private readonly ILogger<GetUserDataEndpoint> _logger;

    public GetUserDataEndpoint(ILogger<GetUserDataEndpoint> logger)
    {
        _logger = logger;
    }

    [HttpGet("/Account/GetUserData")]
    public override ActionResult Handle()
    {
        try
        {
            string? value = HttpContext.Session.GetString("user");
            _logger.LogDebug(HttpContext.Session.GetString("user"));
            if (value is null) return Ok(new ErrorModel{Message = "User is not Authorized", StatusCode = StatusCodes.UserIsNotAuthorized});
            return Ok(value);
        }
        catch (Exception)
        {
            return Ok(new ErrorModel{Message = "User is not Authorized", StatusCode = StatusCodes.UserIsNotAuthorized});
        }
    }
}
