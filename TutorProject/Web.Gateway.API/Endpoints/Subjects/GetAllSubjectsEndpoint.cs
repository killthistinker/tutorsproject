﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Web.Gateway.Interfaces.Contracts;

namespace Web.Gateway.API.Endpoints.Subjects;

public class GetAllSubjectsEndpoint : EndpointBaseAsync.WithoutRequest.WithActionResult
{
    private readonly ISubjectService _subjectService;

    public GetAllSubjectsEndpoint(ISubjectService subjectService)
    {
        _subjectService = subjectService;
    }

    [HttpGet("/Subjects/All")]
    public override async Task<ActionResult> HandleAsync(CancellationToken cancellationToken = new CancellationToken())
    {
        var response = await _subjectService.GetSubjects();
        return Ok(response);
    }
}