﻿using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using Web.Gateway.Interfaces.Contracts;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Web.Gateway.API.Endpoints.Tutor;

public class CreateTutorEndpoint : EndpointBaseAsync.WithRequest<TutorCreateDto>.WithActionResult<DefaultResponseObject<StatusCodes>>
{
    private readonly ITutorService _tutorService;

    public CreateTutorEndpoint(ITutorService tutorService)
    {
        _tutorService = tutorService;
    }

    [HttpPost("/Tutors/Create")]
    public override async Task<ActionResult<DefaultResponseObject<StatusCodes>>> HandleAsync(TutorCreateDto request, CancellationToken cancellationToken = new CancellationToken())
    {
        var response = await _tutorService.AddTutor(request);
        return Ok(response);
    }
}