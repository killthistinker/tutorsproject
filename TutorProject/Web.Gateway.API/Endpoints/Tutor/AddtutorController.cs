﻿using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib.TutorDtos;
using Responses.Helpers;
using Web.Gateway.Interfaces.Contracts;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Web.Gateway.API.Endpoints.Tutor;

public class AddTutorController : ControllerBase
{
    private readonly ITutorApplicationService _service;
 
    public AddTutorController(ITutorApplicationService service)
    {
        _service = service;
    }

    [HttpPost("/Tutor/Add")]
    public async Task<ActionResult<StatusCodes>> Handle(TutorAddDto request)
    {
        var email = HttpContext.Session.GetString("user");
        if (email is null) return Ok(StatusCodes.UserNotFound);
        
        request = request.MapEmail(email);
        var response = await _service.CreateTutor(request);

        return Ok(response);
    }
}