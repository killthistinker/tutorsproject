using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using Responses.Payments;
using Web.Gateway.Interfaces.Contracts;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Web.Gateway.API.Endpoints.Payment;

public class PaymentsEndpoint : EndpointBaseAsync.WithoutRequest.WithActionResult<DefaultResponseObject<List<Transaction>>>
{
    private readonly IPaymentService _paymentService;
    private readonly ILogger<PaymentsEndpoint> _logger;

    public PaymentsEndpoint(IPaymentService paymentService, ILogger<PaymentsEndpoint> logger)
    {
        _paymentService = paymentService;
        _logger = logger;
    }

    [HttpGet("/Pay/GetPayments")]
    public override async Task<ActionResult<DefaultResponseObject<List<Transaction>>>> HandleAsync(CancellationToken cancellationToken = new CancellationToken())
    {
        try
        {
            var email = HttpContext.Session.GetString("user");
            if (email is null) return Ok(Responses.Enums.StatusCodes.UserNotFound);
            GetPaymentDto dto = new GetPaymentDto { Email = email };
            var response = await _paymentService.GetListPayments(dto);
            
            return Ok(response);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Сработал cath.");
            return Ok(StatusCodes.ServiceIsUnavailable);
        }
    }
}
