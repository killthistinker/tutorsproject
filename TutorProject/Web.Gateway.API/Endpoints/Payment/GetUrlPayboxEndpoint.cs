using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Responses.DataTransferLib;
using Responses.Payments;
using Web.Gateway.Interfaces.Contracts;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Web.Gateway.API.Endpoints.Payment;

public class GetUrlPayboxEndpoint : EndpointBaseAsync.WithRequest<PaymentCourseDto>.WithActionResult<DefaultResponseObject<PayResponse>>
{
    private readonly IPaymentService _paymentService;
    private readonly ILogger<GetUrlPayboxEndpoint> _logger;

    public GetUrlPayboxEndpoint(IPaymentService paymentService, ILogger<GetUrlPayboxEndpoint> logger)
    {
        _paymentService = paymentService;
        _logger = logger;
    }

    [HttpPost("/Pay/GetUrl")]
    public override async Task<ActionResult<DefaultResponseObject<PayResponse>>> HandleAsync(PaymentCourseDto request, CancellationToken cancellationToken = new CancellationToken())
    {
        try
        {
            var email = HttpContext.Session.GetString("user");
            if (email is null) return Ok(Responses.Enums.StatusCodes.UserNotFound);
            request.Email = email;
            var response = await _paymentService.GetPagePaybox(request);
            
            return Ok(response);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Сработал cath.");
            return Ok(StatusCodes.ServiceIsUnavailable);
        }
    }
}