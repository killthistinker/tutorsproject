using NLog;
using NLog.Web;
using Web.Gateway.Infrastructure.Services;
using Web.Gateway.Interfaces.Contracts;
using Web.Gateway.API;

var logger = NLog.LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
try
{
    var builder = WebApplication.CreateBuilder(args);
    var services = builder.Services;
// Add services to the container.

    services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    services.AddEndpointsApiExplorer();
    services.AddSwaggerGen();
    services.AddTransient<IUserService, UserService>();
    services.AddTransient<IRoomService, RoomService>();
    services.AddTransient<IScheduleService, ScheduleService>();
    services.AddTransient<ITutorService, TutorService>();
    services.AddTransient<ITutorApplicationService, TutorApplicationService>();
    services.AddTransient<ISubjectService, SubjectService>();
    services.AddTransient<IPaymentService, PaymentService>();
    builder.Services.AddDistributedMemoryCache();
    services.AddHttpClients(builder.Configuration);

    builder.Services.AddSession(options =>
    {
        options.Cookie.Name = "authToken";
        options.IdleTimeout = TimeSpan.FromMinutes(60);
        options.Cookie.IsEssential = true;
        options.Cookie.SameSite = SameSiteMode.Strict;
    });
    var app = builder.Build();

// Configure the HTTP request pipeline.
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseHttpsRedirection();
    app.UseAuthorization();
    app.UseSession();
    app.MapControllers();
    app.UseCors("CorsPolicy");
    app.Run();
}
catch (Exception e)
{
    logger.Error(e, "Остановка программы");
}
finally
{
    NLog.LogManager.Shutdown();
}
