﻿using Responses.DataTransferLib;
using Responses.DataTransferLib.ScheduleDTO;
using Responses.Enums;

namespace Web.Gateway.Interfaces.Contracts;

public interface IScheduleService
{
    Task<DefaultResponseObject<EmptyPeriodDto>> GetGetEmptyPeriod();
    Task<DefaultResponseObject<StatusCodes>> AddAvailablePeriod(SelectedHoursDto selectedHours);
}