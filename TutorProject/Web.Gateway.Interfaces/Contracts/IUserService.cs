﻿using Responses.AccountModels;
using Responses.AccountModels.Responses;
using Responses.DataTransferLib;
using Responses.Enums;

namespace Web.Gateway.Interfaces.Contracts;

public interface IUserService
{
    Task<DefaultResponseObject<DefaultResponse>> RegisterAsync(UserRegisterDto model, CancellationToken cancellationToken);
    Task<DefaultResponseObject<AuthenticateResponse>> LoginAsync(UserAuthDto model, CancellationToken cancellationToken);
    Task<DefaultResponseObject<DefaultResponse>> EditAsync(EditUserDto model, CancellationToken cancellationToken);
}