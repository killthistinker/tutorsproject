﻿using Microsoft.Extensions.Logging;
using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using Responses.Enums;
using Responses.Extensions;
using Web.Gateway.Interfaces.Contracts;

namespace Web.Gateway.Infrastructure.Services;

public class TutorService : ITutorService
{
    private readonly HttpClient _httpClient;
    private readonly ILogger<UserService> _logger;

    public TutorService(HttpClient httpClient, ILogger<UserService> logger)
    {
        _httpClient = httpClient;
        _logger = logger;
    }

    public async Task<DefaultResponseObject<StatusCodes>> AddTutor(TutorCreateDto model)
    {
        try
        {
            var response =
                await _httpClient.PostAndReturnResponseAsync<TutorCreateDto, StatusCodes>(model,
                    "/Tutors/Create");
            return response;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Сработал cath.");
            return new DefaultResponseObject<StatusCodes> { Value = StatusCodes.ServiceIsUnavailable };
        }
    }

    public async Task<DefaultResponseObject<StatusCodes>> RefuseTutor(RefuseTutorDto model)
    {
        try
        {
            var response =
                await _httpClient.PostAndReturnResponseAsync<RefuseTutorDto, StatusCodes>(model,
                    "/Tutors/Refuse");
            return response;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Сработал cath.");
            return new DefaultResponseObject<StatusCodes> { Value = StatusCodes.ServiceIsUnavailable };
        }
    }
}