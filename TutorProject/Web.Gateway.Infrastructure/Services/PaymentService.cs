using Microsoft.Extensions.Logging;
using Responses.DataTransferLib;
using Responses.Extensions;
using Responses.Payments;
using Web.Gateway.Interfaces.Contracts;

namespace Web.Gateway.Infrastructure.Services;

public class PaymentService : IPaymentService
{
    private readonly HttpClient _httpClient;
    private readonly ILogger<PaymentService> _logger;

    public PaymentService(HttpClient httpClient, ILogger<PaymentService> logger)
    {
        _httpClient = httpClient;
        _logger = logger;
    }
    public async Task<DefaultResponseObject<PayResponse>> GetPagePaybox(PaymentCourseDto model)
    {
        try
        {
            var response = await _httpClient.PostAndReturnResponseAsync<PaymentCourseDto, PayResponse>( model, "/Payment/GetPage");
            return response;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Произошла ошибка");
            throw;
        }
    }
    public async Task<DefaultResponseObject<List<Transaction>>> GetListPayments(GetPaymentDto model)
    {
        try
        {
            var response = await _httpClient.PostAndReturnResponseAsync<GetPaymentDto, List<Transaction>>( model, "/Payment/GetPayments");
            return response;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Произошла ошибка");
            throw;
        }
    }
}