using Microsoft.Extensions.Logging;
using Responses.AccountModels;
using Responses.AccountModels.Responses;
using Responses.DataTransferLib;
using Responses.Enums;
using Responses.Extensions;
using Web.Gateway.Interfaces.Contracts;

namespace Web.Gateway.Infrastructure.Services;

public class UserService : IUserService
{
    private readonly HttpClient _httpClient;
    private readonly ILogger<UserService> _logger;

    public UserService(HttpClient httpClient, ILogger<UserService> logger)
    {
        _httpClient = httpClient;
        _logger = logger;
    }

    public async Task<DefaultResponseObject<DefaultResponse>> RegisterAsync(UserRegisterDto model,
        CancellationToken cancellationToken)
    {
        try
        {
            var response =
                await _httpClient.PostAndReturnResponseAsync<UserRegisterDto, DefaultResponse>(model,
                    "/Account/Register");
            return response;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Произошла ошибка");
            throw;
        }
    }

    public async Task<DefaultResponseObject<AuthenticateResponse>> LoginAsync(UserAuthDto model,
            CancellationToken cancellationToken)
        {
            try
            {
                var response =
                    await _httpClient.PostAndReturnResponseAsync<UserAuthDto, AuthenticateResponse>(model,
                        "/Account/Login");
                return response;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Произошла ошибка");
                throw;
            }
        }

    public async Task<DefaultResponseObject<DefaultResponse>> EditAsync(EditUserDto model, CancellationToken cancellationToken)
    {
        try
        {
            var response = await _httpClient.PostAndReturnResponseAsync<EditUserDto,DefaultResponse>(model, "/Account/EditProfile");
            return response;
        }
        catch(Exception e)
        {
            _logger.LogError(e, "Произошла ошибка");
            throw;
        }        
    }
}
