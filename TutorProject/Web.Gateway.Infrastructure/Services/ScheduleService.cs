﻿using Responses.AccountModels;
using Responses.DataTransferLib;
using Responses.DataTransferLib.ScheduleDTO;
using Responses.Enums;
using Responses.Extensions;
using Web.Gateway.Interfaces.Contracts;

namespace Web.Gateway.Infrastructure.Services;

public class ScheduleService : IScheduleService
{
    private readonly HttpClient _httpClient;

    public ScheduleService(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }
    public async Task<DefaultResponseObject<EmptyPeriodDto>> GetGetEmptyPeriod()
    {
        try
        {
            var response = await _httpClient.GetAndReturnResponseAsync<EmptyPeriodDto>("/GetPeriodGenerator/GetEmptyPeriod");
            return response;
        }
        catch (Exception e)
        {
            throw;
        }
    }

    public async Task<DefaultResponseObject<StatusCodes>> AddAvailablePeriod(SelectedHoursDto model)
    {
        try
        {
            var response = await _httpClient.PostAndReturnResponseAsync<SelectedHoursDto, StatusCodes>(model, "/AddTutorPeriod/AddAvailablePeriod");
            return response;
        }
        catch (Exception e)
        {
            throw;
        }
    }
}