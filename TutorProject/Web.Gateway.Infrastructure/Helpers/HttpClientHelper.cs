﻿using System.Net.Http.Headers;
using Newtonsoft.Json;
using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using StatusCodes = Responses.Enums.StatusCodes;

namespace Web.Gateway.Infrastructure.Helpers;

public static class HttpClientHelper
{
    public async static Task<DefaultResponseObject<StatusCodes>> PostFilesAndReturnResultAsync(this HttpClient client, TutorAddDto request, string uri)
    {
        MultipartFormDataContent form = new MultipartFormDataContent();
        if (request.Email != null) form.Add(new StringContent(request.Email), "email");
        if (request.Description != null) form.Add(new StringContent(request.Description), "description");
        if (request.Selection != null)
            foreach (var selection in request.Selection)
            {
                form.Add(new StringContent(selection), "selection");
            }

        foreach (var file in request.Files)
        {
            form.Add(new StreamContent(file.OpenReadStream()), "files", file.FileName);
            form.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                { Name = "files", FileName = file.FileName };
        }
        
        var message = new HttpRequestMessage()
        {
            Method = HttpMethod.Post,
            RequestUri = new Uri(client.BaseAddress!, uri),
            Content = form
        };

        return await client.ExchangeAsync<StatusCodes>(message);
    }
    
    private async static Task<DefaultResponseObject<TResponse>> ExchangeAsync<TResponse>(this HttpClient client, HttpRequestMessage message)
    {
        try
        {
            var response = await client.SendAsync(message);
            response.EnsureSuccessStatusCode();
            string dataAsString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            var result = JsonConvert.DeserializeObject<DefaultResponseObject<TResponse>>(dataAsString);
            if (result == null) throw new InvalidCastException($"Cast to {typeof(DefaultResponseObject<TResponse>)} is dropped");
            return result;
        }
        catch (Exception ex)
        {
            return new DefaultResponseObject<TResponse>() { ErrorModel =  new ErrorModel{StatusCode  = StatusCodes.ServiceIsUnavailable, Message = $"{ex.Message}"}};
        }
    }
}