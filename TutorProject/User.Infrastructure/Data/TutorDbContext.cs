﻿using Microsoft.EntityFrameworkCore;
using User.Domain.Entities;
using User.Infrastructure.Data.DbMap;

namespace User.Infrastructure.Data;

public sealed class TutorDbContext : DbContext
{
    public DbSet<Domain.Entities.User>? Users { get; set; }
    public DbSet<Room>? Rooms { get; set; }
    
    public TutorDbContext(DbContextOptions<TutorDbContext> options) : base(options)
    {
        Database.EnsureCreated();
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
    }
}