﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using User.Domain.Entities;

namespace User.Infrastructure.Data.DbMap;

public class RoomDbMap : IEntityTypeConfiguration<Domain.Entities.Room>
{
    private readonly ModelBuilder _builder;

    public RoomDbMap(ModelBuilder builder)
    {
        _builder = builder;
    }

    public void Configure(EntityTypeBuilder<Room> builder)
    {
        builder.Property(p => p.Name).HasColumnType("VARCHAR(100)").IsRequired();
        builder.Property(p => p.DateCreate).HasColumnType("VARCHAR(50)").IsRequired();
        builder.Property(p => p.Address).HasColumnType("VARCHAR(13)").IsRequired();
        builder.Property(p => p.CreatorId).HasColumnType("INTEGER").IsRequired();
    }
}