﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace User.Infrastructure.Data.DbMap;

public class UserDbMap : IEntityTypeConfiguration<Domain.Entities.User>
{
    
    private readonly ModelBuilder _builder;

    public UserDbMap(ModelBuilder builder)
    {
        _builder = builder;
    }
    public void Configure(EntityTypeBuilder<Domain.Entities.User> builder)
    {
        builder.ToTable("Users");

        builder.Property(u => u.FirstName).IsRequired().HasMaxLength(100);
        builder.Property(u => u.LastName).IsRequired().HasMaxLength(100);
        builder.Property(u => u.RegisterDate).IsRequired().HasMaxLength(100);
        builder.Property(u => u.DateOfBirth).HasMaxLength(100);
        builder.Property(u => u.IsActive).IsRequired();
        builder.Property(u => u.Balance).HasColumnType("decimal(18, 2)").IsRequired();

        // Конфигурация для столбца "Email", если нужен
        builder.Property(u => u.Email).IsRequired().HasMaxLength(256);
        builder.HasIndex(u => u.Email).IsUnique();
        // Конфигурация для столбца "PhoneNumber", если нужен
        builder.Property(u => u.PhoneNumber).HasMaxLength(20);
        builder.HasIndex(u => u.PhoneNumber).IsUnique();
        builder.Property(u => u.Id);
        builder.Property(u => u.TwoFactorEnabled);
        builder.Property(u => u.PhoneNumberConfirmed);
        builder.Property(u => u.NormalizedUserName);
        builder.Property(u => u.AccessFailedCount);
        builder.Property(u => u.UserName);
        builder.Property(u => u.SecurityStamp);
        builder.Property(u => u.PasswordHash);
        builder.Property(u => u.NormalizedEmail);
        builder.Property(u => u.LockoutEnd);
        builder.Property(u => u.LockoutEnabled);
        builder.Property(u => u.EmailConfirmed);
        builder.Property(u => u.ConcurrencyStamp);
    }
}