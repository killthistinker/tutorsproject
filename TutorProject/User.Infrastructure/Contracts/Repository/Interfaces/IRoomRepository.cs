﻿using Responses.Contracts.Repository;
using User.Domain.Entities;

namespace User.Infrastructure.Contracts.Repository.Interfaces;

public interface IRoomRepository : IRepository<Room>
{
    
}