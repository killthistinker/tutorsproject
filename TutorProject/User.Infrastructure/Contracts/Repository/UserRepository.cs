﻿using Microsoft.EntityFrameworkCore;
using User.Infrastructure.Contracts.Repository.Interfaces;
using User.Infrastructure.Data;

namespace User.Infrastructure.Contracts.Repository;

public class UserRepository : IUserRepository
{
    private readonly TutorDbContext _context;

    public UserRepository(TutorDbContext context)
    {
        _context = context;
    }

    public IEnumerable<Domain.Entities.User>? GetAll()
    {
        var users = _context.Users;
        return users ?? null;
    }

    public async Task CreateAsync(Domain.Entities.User item) => await _context.Users.AddAsync(item);

    public void Create(Domain.Entities.User item) =>
    _context.Users?.Add(item);
    

    public void Update(Domain.Entities.User item) =>
    _context.Users?.Update(item);
    

    public void Remove(Domain.Entities.User item) =>
    _context.Users?.Remove(item);

    public void Save() => _context.SaveChanges();

    public async Task SaveAsync() => await _context.SaveChangesAsync();
 

    public async Task<Domain.Entities.User?>? GetFirstOrDefaultByIdAsync(long id)
    {
        if (_context.Users == null) return null;
        
        var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
        return user ?? null;

    }

    public async Task<Domain.Entities.User?>? GetFirstOrDefaultByEmailAsync(string email)
    {
        if (_context.Users == null) return null;
        var user = await _context.Users.FirstOrDefaultAsync(u => u.Email == email);
        return user ?? null;
    }
}