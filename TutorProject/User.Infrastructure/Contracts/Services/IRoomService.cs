﻿using Responses.DataTransferLib;
using Responses.Enums;
using User.Domain.Entities;

namespace User.Infrastructure.Contracts.Services;

public interface IRoomService
{
    public Task<DefaultResponseObject<StatusCodes>> Add(RoomModelRequest model);
    public DefaultResponseObject<IEnumerable<RoomDto>?> GetAll();
}