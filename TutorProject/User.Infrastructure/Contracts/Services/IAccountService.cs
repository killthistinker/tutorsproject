using Ardalis.Result;
using Responses.AccountModels;
using Responses.AccountModels.Responses;
using Responses.DataTransferLib;

namespace User.Infrastructure.Contracts.Services;

public interface IAccountService
{
   public Task<Result<DefaultResponseObject<AuthenticateResponse>>> Authenticate(UserAuthDto model);
   public Task<Result<DefaultResponseObject<DefaultResponse>>> Register(UserRegisterDto userModel);
}