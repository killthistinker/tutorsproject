﻿using Responses.DataTransferLib.TutorDtos;

namespace Responses.Helpers;

public static class TutorAddDtoMapper
{
    public static TutorAddDto MapEmail(this TutorAddDto model, string email)
    {
        model.Email = email;
        return model;
    }
}