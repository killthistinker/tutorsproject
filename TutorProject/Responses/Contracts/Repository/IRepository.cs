﻿namespace Responses.Contracts.Repository;

public interface IRepository<T>
{
    public IEnumerable<T>? GetAll();
    public Task CreateAsync(T item);
    public void Create(T item);
    public void Update(T item);
    public void Remove(T item);
    public void Save(); 
    public Task SaveAsync();
}