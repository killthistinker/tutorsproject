﻿namespace Responses.ViewModels;

public class SubjectsDto
{
  public IEnumerable<string> Subjects { get; set; }
}