﻿using Ardalis.Result;

namespace Responses.DataTransferLib;

public class DefaultResponseObject<T>
{
    public T? Value { get; set; }
    public bool IsSuccess { get; set; }
    public ErrorModel? ErrorModel { get; set; }
    public ValidationError[]? ValidationErrors { get; set; }
}