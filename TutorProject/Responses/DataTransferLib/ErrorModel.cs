using Responses.Enums;

namespace Responses.DataTransferLib;
public class ErrorModel
{
    public StatusCodes StatusCode { get; set; }
    public string Title { get; set; }
    public string Message { get; set; }
}