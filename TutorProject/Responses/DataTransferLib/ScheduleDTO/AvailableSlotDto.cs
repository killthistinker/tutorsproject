﻿namespace Responses.DataTransferLib.ScheduleDTO;

public class AvailableSlotDto
{
    public List<AvailableHourDto>? Hours { get; set; }
}