﻿using System.Text.Json.Serialization;

namespace Responses.DataTransferLib.ScheduleDTO;

public class SelectedHoursDto
{
    [JsonPropertyName("hours")]
    public string[] HourStrings { get; set; }
}