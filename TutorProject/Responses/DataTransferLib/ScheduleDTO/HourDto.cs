﻿namespace Responses.DataTransferLib.ScheduleDTO;

public class HourDto
{
    public string HourName { get; set; }
    
    public bool Available { get; set; } = false;
}