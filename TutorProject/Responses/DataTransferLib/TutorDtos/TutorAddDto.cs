﻿using Microsoft.AspNetCore.Http;

namespace Responses.DataTransferLib.TutorDtos;

public class TutorAddDto
{
    public List<IFormFile> Files { get; set; }
    public List<string>? Selection  { get; set; }
    public string? Description { get; set; }
    public List<string>? ImagesPath { get; set; }
    public string? Email { get; set; }
}