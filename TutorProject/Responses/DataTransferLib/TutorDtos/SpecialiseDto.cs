﻿namespace Responses.DataTransferLib.TutorDtos;

public class SpecialiseDto
{
    public string Name { get; set; }
    public string SubjectName { get; set; }
}