﻿namespace Responses.DataTransferLib.TutorDtos;

public class RefuseTutorDto
{
    public string Message { get; set; }
    public string Email { get; set; }
}