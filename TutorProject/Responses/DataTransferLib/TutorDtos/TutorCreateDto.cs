﻿namespace Responses.DataTransferLib.TutorDtos;

public class TutorCreateDto
{
    public string Email { get; set; }
    public IEnumerable<SpecialiseDto> Specializes { get; set; }
}