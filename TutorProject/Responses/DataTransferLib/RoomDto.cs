﻿namespace Responses.DataTransferLib;

public class RoomDto
{
    public string Name { get; set; }
    public string DateCreate { get; set; }
    public string Creator { get; set; }
    public string Address { get; set; }
}