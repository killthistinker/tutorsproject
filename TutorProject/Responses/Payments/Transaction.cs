namespace Responses.Payments;

public class Transaction
{
    public string Email { get; set; }
    public int Amount { get; set; }
    public string LessonName { get; set; }
    public string Status { get; set; }
    public DateTime CreateDate { get; set; }
    public int PayboxId { get; set; }
    public string? Card { get; set; }
}