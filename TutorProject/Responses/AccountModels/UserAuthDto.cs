using System.ComponentModel.DataAnnotations;

namespace Responses.AccountModels;

public class UserAuthDto
{
    [Required]
    public string Email { get; set; }
    [Required]
    public string Password { get; set; }
}