﻿namespace Responses.AccountModels.Responses;

public class AuthenticateResponse
{
    public string? Email { get; set; }
    public string? Token { get; set; }
}