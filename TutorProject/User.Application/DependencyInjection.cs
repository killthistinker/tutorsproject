﻿using System.Reflection;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using User.Application.Mappings;
using User.Application.Services;
using User.Infrastructure.Contracts.Services;

namespace User.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddTransient<IAccountService, AccountService>();
        serviceCollection.AddTransient<IRoomService, RoomService>();
        serviceCollection.AddTransient<IEditProfileService, EditProfileService>();
        serviceCollection.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
        serviceCollection.AddAutoMapper(typeof(UserMapper) , typeof(DefaultResponseObjectProfile), typeof(RoomMapper));
        return serviceCollection;
    }
}