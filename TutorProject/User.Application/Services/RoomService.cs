﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Responses.DataTransferLib;
using Responses.Enums;
using User.Domain.Entities;
using User.Infrastructure.Contracts.Repository.Interfaces;
using User.Infrastructure.Contracts.Services;

namespace User.Application.Services;

public class RoomService : IRoomService
{
    private readonly IMapper _mapper;
    private readonly IRoomRepository _repository;
    private readonly IUserRepository _userRepository;
    private readonly ILogger<RoomService> _logger;

    public RoomService(IMapper mapper, IRoomRepository repository, IUserRepository userRepository, ILogger<RoomService> logger)
    {
        _mapper = mapper;
        _repository = repository;
        _userRepository = userRepository;
        _logger = logger;
    }

    public async Task<DefaultResponseObject<StatusCodes>> Add(RoomModelRequest model)
    {
        try
        {
            var user = await _userRepository.GetFirstOrDefaultByEmailAsync(model.Email);
            if (user is null) return new DefaultResponseObject<StatusCodes> { Value = StatusCodes.UserNotFound };
        
            var room = _mapper.Map<Room>(model);
            if(room is null) return new DefaultResponseObject<StatusCodes> { Value = StatusCodes.ServiceIsUnavailable };
            room.CreatorId = user.Id;
            
            await _repository.CreateAsync(room);
            await _repository.SaveAsync();
            return new DefaultResponseObject<StatusCodes> { Value = StatusCodes.ServiceOk };
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Сработал cath");
            return new DefaultResponseObject<StatusCodes> { Value = StatusCodes.ServiceIsUnavailable };
        }
    }

    public DefaultResponseObject<IEnumerable<RoomDto>?> GetAll()
    {
        try
        {
            IEnumerable<Room>? rooms = _repository.GetAll();
            if (rooms is null) return new DefaultResponseObject<IEnumerable<RoomDto>?>
                {ErrorModel = new ErrorModel {Message = "Список комнат пуст",StatusCode = StatusCodes.RoomsNotFound}};
            var roomModels = rooms.Select(room => _mapper.Map<RoomDto>(room)).ToList();
           
            return new DefaultResponseObject<IEnumerable<RoomDto>?>{Value = roomModels};
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Сработал cath");
            return new DefaultResponseObject<IEnumerable<RoomDto>?>
                {ErrorModel = new ErrorModel {Message = "Сервис временно недоступен",StatusCode = StatusCodes.ServiceIsUnavailable}};
        }
    }
}