﻿using AutoMapper;
using Responses.AccountModels;
using Responses.AccountModels.Responses;
using Responses.DataTransferLib;

namespace User.Application.Mappings;

public class UserMapper : Profile
{
    public UserMapper()
    {
        CreateMap<UserRegisterDto, Domain.Entities.User>()
            .ForMember(dst => dst.Email, opt => opt.MapFrom(src => src.Email))
            .ForMember(dst => dst.PhoneNumber, opt => opt.MapFrom(src => src.Phone))
            .ForMember(dst => dst.DateOfBirth, opt => opt.MapFrom(src => src.DateBirth))
            .ForMember(dst => dst.FirstName, opt => opt.MapFrom(src => src.FirstName))
            .ForMember(dst => dst.LastName, opt => opt.MapFrom(src => src.LastName))
            .ForMember(dst => dst.Id, opt => opt.Ignore())
            .ForMember(dst => dst.RegisterDate, opt => opt.MapFrom(src => DateTime.Now))
            .ForMember(dst => dst.UserName, opt => opt.MapFrom(src => src.Email));

        CreateMap<Domain.Entities.User, AuthenticateResponse>()
            .ForMember(dst => dst.Email, opt => opt.MapFrom(src => src.Email))
            .ForMember(dst => dst.Token, opt => opt.Ignore());

        CreateMap<EditUserDto, Domain.Entities.User>()
            .ForMember(dst => dst.PhoneNumber, opt => opt.MapFrom(src => src.Phone))
            .ForMember(dst => dst.DateOfBirth, opt => opt.MapFrom(src => src.DateBirth))
            .ForMember(dst => dst.FirstName, opt => opt.MapFrom(src => src.Name))
            .ForMember(dst => dst.LastName, opt => opt.MapFrom(src => src.Surname))
            .ForMember(dst => dst.Id, opt => opt.Ignore())
            .ForMember(dst => dst.RegisterDate, opt => opt.MapFrom(src => DateTime.Now))
            .ForMember(dst => dst.UserName, opt => opt.MapFrom(src => src.Email));
    }
}