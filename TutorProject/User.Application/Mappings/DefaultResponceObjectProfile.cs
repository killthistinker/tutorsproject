﻿using Ardalis.Result;
using AutoMapper;
using Responses.AccountModels;
using Responses.AccountModels.Responses;
using Responses.DataTransferLib;

namespace User.Application.Mappings;

public class DefaultResponseObjectProfile : Profile
{
    public DefaultResponseObjectProfile()
    {
        CreateMap(typeof(Result<Task<DefaultResponseObject<DefaultResponse>>>), typeof(DefaultResponseObject<DefaultResponse>));
        CreateMap(typeof(Result<Task<DefaultResponseObject<AuthenticateResponse>>>), typeof(DefaultResponseObject<AuthenticateResponse>));
    }
}