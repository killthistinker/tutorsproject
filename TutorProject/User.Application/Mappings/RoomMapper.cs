﻿using AutoMapper;
using Responses.DataTransferLib;
using User.Domain.Entities;

namespace User.Application.Mappings;

public class RoomMapper : Profile
{
    public RoomMapper()
    {
        CreateMap<RoomModelRequest, Room>()
            .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name))
            .ForMember(dst => dst.DateCreate, opt => opt.MapFrom(src => DateTime.Now))
            .ForMember(dst => dst.Address, opt => opt.MapFrom(src => Guid.NewGuid().ToString()));
        CreateMap<Room, RoomDto>()
            .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name))
            .ForMember(dst => dst.DateCreate, opt => opt.MapFrom(src => src.DateCreate))
            .ForMember(dst => dst.Creator,
                opt => opt.MapFrom(src => $"{src.Creator.FirstName} {src.Creator.LastName}"))
            .ForMember(dst => dst.Address, opt => opt.MapFrom(src => src.Address));
        CreateMap<long, Room>()
            .ForMember(dst => dst.CreatorId, opt => opt.MapFrom(src => src));
    }
}