using Payment.Domain.Entities;

namespace Payment.Interfaces;

public interface IPaymentRepository
{
    public void CreateLog(TransactionLog log);
    public void UpdateLog(TransactionLog log);
    public List<TransactionLog> GetAll();
    public void Save();
}