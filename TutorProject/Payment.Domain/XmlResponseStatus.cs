using System.Xml.Serialization;

namespace Payment.Domain;


[XmlRoot(ElementName = "response")]
public class XmlResponseStatus
{
    [XmlElement(ElementName = "pg_status")]
    public string Status { get; set; }
    [XmlElement(ElementName = "pg_payment_id")]
    public int PaymentId { get; set; }
    [XmlElement(ElementName = "pg_transaction_status")]
    public string TransactionStatus { get; set; }
    [XmlElement(ElementName = "pg_testing_mode")]
    public int TestingMode { get; set; }
    [XmlElement(ElementName = "pg_payment_method")]
    public string PaymentMethod { get; set; }
    [XmlElement(ElementName = "pg_amount")]
    public int Amount { get; set; }
    [XmlElement(ElementName = "pg_currency")]
    public string Currency { get; set; }
    [XmlElement(ElementName = "pg_captured")]
    public int Captured { get; set; }
    [XmlElement(ElementName = "pg_create_date")]
    public string CreateDate { get; set; }
    [XmlElement(ElementName = "pg_can_reject")]
    public int CanReject { get; set; }
    [XmlElement(ElementName = "pg_card_pan")]
    public string Card { get; set; }
    [XmlElement(ElementName = "pg_salt")]
    public string Salt { get; set; }
    [XmlElement(ElementName = "pg_sig")]
    public string Sig { get; set; }
}
