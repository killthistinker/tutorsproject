﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Payment.Domain
{
    [XmlRoot(ElementName = "response")]
    public class XmlResponseUrl
    {
        [XmlElement(ElementName = "pg_status")]
        public string Status { get; set; }
        [XmlElement(ElementName = "pg_payment_id")]
        public int PaymentId { get; set; }
        [XmlElement(ElementName = "pg_redirect_url")]
        public string RedirectUrl { get; set; }
        [XmlElement(ElementName = "pg_redirect_url_type")]
        public string RedirectUrlType { get; set; }
        [XmlElement(ElementName = "pg_salt")]
        public string Salt { get; set; }
        [XmlElement(ElementName = "pg_sig")]
        public string Sig { get; set; }
        [XmlElement(ElementName = "pg_error_description")]
        public string? ErrorDescription { get; set; }
    }
}
