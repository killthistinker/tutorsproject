namespace WebApplication1.ViewModels;

public class EntitiesViewModel
{
    public List<SubjectsViewModel>? Subjects { get; set; } = new List<SubjectsViewModel>();
    public SubjectsViewModel SubjectsViewModel { get; set; }
    public List<SpecialitiesViewModel>? Specialities { get; set; }
    public List<SpecialitiesLevelViewModel>? SpecialitiesLevel { get; set; }
}
