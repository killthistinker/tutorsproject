﻿namespace WebApplication1.ViewModels.Applications;

public class ApplicationsViewModel
{
    public IEnumerable<ApplicationViewModel> Applications { get; set; }
}