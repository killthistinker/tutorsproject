﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApplication1.Entities;

namespace WebApplication1.Data.DbMap;

public class SpecialityDbMap : IEntityTypeConfiguration<Speciality>
{
    public void Configure(EntityTypeBuilder<Speciality> builder)
    {
        builder.Property(p => p.Name).HasColumnType("VARCHAR(50)").IsRequired();
        builder.HasIndex(p => p.Name).IsUnique();
        builder.Property(p => p.Description).HasColumnType("VARCHAR(500)").IsRequired();
        builder.Property(p => p.SubjectName).HasColumnType("VARCHAR(50)").IsRequired();
        builder.HasIndex(p => p.SubjectName).IsUnique();
    }
}