﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApplication1.Entities;

namespace WebApplication1.Data.DbMap;

public class ApplicationSpecialisesDbMap : IEntityTypeConfiguration<ApplicationSpecialise>
{
    public void Configure(EntityTypeBuilder<ApplicationSpecialise> builder)
    {
        builder.Property(p => p.Name).HasColumnType("VARCHAR(50)").IsRequired();
    }
}