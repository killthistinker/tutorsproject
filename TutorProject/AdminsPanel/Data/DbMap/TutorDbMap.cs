﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebApplication1.Entities;

namespace WebApplication1.Data.DbMap;

public class TutorDbMap : IEntityTypeConfiguration<TutorApplication>
{
    public void Configure(EntityTypeBuilder<TutorApplication> builder)
    {
        builder.Property(p => p.Email).HasColumnType("VARCHAR(50)").IsRequired();
        builder.HasIndex(p => p.Email).IsUnique();
        builder.Property(p => p.Description).HasColumnType("VARCHAR(1000)").IsRequired();
        builder.HasMany(x => x.ApplicationSpecialises).WithOne(x => x.TutorApplication).HasForeignKey(x => x.Id);
        builder.HasMany(x => x.Certificates).WithOne(x => x.TutorApplication).HasForeignKey(x => x.Id);
    }
}