﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using WebApplication1.Data.DbMap;
using WebApplication1.Entities;

namespace WebApplication1.Data.AdminDb;

public sealed class EntityContext : DbContext
{
    public DbSet<Subject> Subjects { get; set; }
    public DbSet<Speciality> Specialities { get; set; }

    public EntityContext(DbContextOptions<EntityContext> options) : base(options)
    {
        Database.EnsureCreated();
    }
    
    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.Entity<Subject>().HasData(new Subject { Id = "1", Name = "Английский язык" }, new Subject { Id = "2", Name = "Алгебра" }, new Subject { Id = "3", Name = "Физика" });
        base.OnModelCreating(builder);
    }
}


