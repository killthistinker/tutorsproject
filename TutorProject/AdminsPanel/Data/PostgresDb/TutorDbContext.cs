﻿using Microsoft.EntityFrameworkCore;
using WebApplication1.Data.DbMap;
using WebApplication1.Entities;

namespace WebApplication1.Data.PostgresDb;

public sealed class TutorDbContext : DbContext
{
    public DbSet<TutorApplication> Applications { get; set; }

    public TutorDbContext(DbContextOptions<TutorDbContext> options) : base(options)
    {
        Database.EnsureCreated();
    }
    
    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
    }
}