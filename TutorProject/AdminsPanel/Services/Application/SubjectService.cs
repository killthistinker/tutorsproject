﻿using Responses.ViewModels;
using WebApplication1.Helpers.Mappings;
using WebApplication1.Repositories.MongoRepository;
using WebApplication1.Services.Application.Interfaces;
using WebApplication1.ViewModels;

namespace WebApplication1.Services.Application;

public class SubjectService : ISubjectService
{
    private readonly IEntityRepository<SubjectsViewModel> _subjectRepository;

    public SubjectService(IEntityRepository<SubjectsViewModel> subjectRepository)
    {
        _subjectRepository = subjectRepository;
    }

    public SubjectsDto GetAllSubjects()
    {
        var subjects = _subjectRepository.GetEntitiesAsync();
        var subjectViewModel = subjects.Map();
        return subjectViewModel;
    }
}