﻿using Responses.DataTransferLib.TutorDtos;
using WebApplication1.Helpers.Mappings;
using WebApplication1.Repositories.MongoRepository;
using WebApplication1.Services.Application.Interfaces;
using WebApplication1.ViewModels;
using WebApplication1.ViewModels.Specializes;

namespace WebApplication1.Services.Application;

public class SpecializeService : ISpecializeService
{
    private readonly IEntityRepository<SpecialitiesViewModel> _repository;
   

    public SpecializeService(IEntityRepository<SpecialitiesViewModel> repository)
    {
        _repository = repository;
    }

    public GetAllSpecialisesViewModel GetAllSpecialisesByValues(List<string> values)
    {
        var specialities =  _repository.GetEntitiesAsync();
        var result = specialities.SpecialisesMapWithValues(values);
        return !result.Speicalities.Any() ? null : result;
    }

    public IEnumerable<SpecialiseDto> GetSpecialisesBySubjects(List<string> subjects)
    {
        if (subjects.Count == 0) return null;
        List<SpecialiseDto> response = new List<SpecialiseDto>();
        foreach (var subject in subjects)
        {
            var subjectModel = _repository.GetEntitiesByName(subject);
            foreach (var model in subjectModel)
            {
                SpecialiseDto subjectDto = new SpecialiseDto { Name = model.Name, SubjectName = model.SubjectName };
                response.Add(subjectDto);
            }
        }

        return response;
    }
}