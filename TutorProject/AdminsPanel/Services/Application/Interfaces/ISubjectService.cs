﻿using Responses.ViewModels;

namespace WebApplication1.Services.Application.Interfaces;

public interface ISubjectService
{
    public SubjectsDto GetAllSubjects();
}