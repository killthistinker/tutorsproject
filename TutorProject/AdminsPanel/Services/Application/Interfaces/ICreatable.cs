﻿namespace WebApplication1.Services.Application.Interfaces
{
    public interface  ICreatable<in T> where T : class
    {
        public Task CreateAsync(T entity);
    }
}