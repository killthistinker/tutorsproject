﻿using Responses.DataTransferLib.TutorDtos;
using WebApplication1.ViewModels.Specializes;

namespace WebApplication1.Services.Application.Interfaces;

public interface ISpecializeService
{
    public GetAllSpecialisesViewModel  GetAllSpecialisesByValues(List<string> values);
    public IEnumerable<SpecialiseDto> GetSpecialisesBySubjects(List<string> subjects);
}