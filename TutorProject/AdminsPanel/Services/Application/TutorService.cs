﻿using Responses.DataTransferLib;
using Responses.DataTransferLib.TutorDtos;
using Responses.Extensions;
using WebApplication1.Services.Application.Interfaces;
using StatusCodes = Responses.Enums.StatusCodes;


namespace WebApplication1.Services.Application;

public class TutorService : ITutorService
{
    private readonly HttpClient _httpClient;

    public TutorService(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<DefaultResponseObject<StatusCodes>> AddTutor(TutorCreateDto model)
    {
        try
        {
            var response = await _httpClient.PostAndReturnResponseAsync<TutorCreateDto, StatusCodes>( model, "/Tutors/Create");
            return response;
        }
        catch (Exception e)
        {
            return new DefaultResponseObject<StatusCodes>{Value = StatusCodes.ServiceIsUnavailable};
        }
    }

    public async Task<DefaultResponseObject<StatusCodes>> RefuseTutor(RefuseTutorDto model)
    {
        try
        {
            var response = await _httpClient.PostAndReturnResponseAsync<RefuseTutorDto, StatusCodes>( model, "/Tutors/Refuse");
            return response;
        }
        catch (Exception e)
        {
            return new DefaultResponseObject<StatusCodes>{Value = StatusCodes.ServiceIsUnavailable};
        }
    }
}