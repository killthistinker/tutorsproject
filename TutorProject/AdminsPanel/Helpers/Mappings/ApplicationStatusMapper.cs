﻿using WebApplication1.Enums;

namespace WebApplication1.Helpers.Mappings;

public static class ApplicationStatusMapper
{
    public static string MapStatus(this ApplicationStatus status)
    {
        string result;
        switch (status)
        {
            case ApplicationStatus.StatusNew:
                result = "Новая заявка";
                break;
            case ApplicationStatus.StatusPassed:
                result = "Одобренная заявка";
                break;
            default: 
                result = "Неодобренная заявка"; 
                break;
        }

        return result;
    }
}