﻿using Responses.ViewModels;
using WebApplication1.ViewModels;

namespace WebApplication1.Helpers.Mappings;

public static class SubjectsViewModelHelper
{
    public static SubjectsDto Map(this IEnumerable<SubjectsViewModel> models)
    {
        List<string> subjectsViewModel = new List<string>();
        foreach (var model in models)
        {
            subjectsViewModel.Add(model.Name);
        }

        return new SubjectsDto { Subjects = subjectsViewModel };
    }
}