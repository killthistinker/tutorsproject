﻿using FluentValidation;
using WebApplication1.ViewModels;

namespace WebApplication1.Validation;

public class EntitiesVMValidator : AbstractValidator<EntitiesViewModel>
{
    public EntitiesVMValidator()
    {
        RuleForEach(p => p.Subjects).SetValidator(new SubjectsVMValidation());
        RuleForEach(p => p.Specialities).SetValidator(new SpecialitiesVMValidator());
    }
}