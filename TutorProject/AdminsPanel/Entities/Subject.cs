﻿namespace WebApplication1.Entities;

public class Subject
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string? Description { get; set; }
}