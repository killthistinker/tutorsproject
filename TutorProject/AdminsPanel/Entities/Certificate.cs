﻿namespace WebApplication1.Entities;

public class Certificate
{
    public long Id { get; set; }
    public string Path { get; set; }
    
    public TutorApplication TutorApplication { get; set; }
    
    public long TutorApplicationId { get; set; }
}