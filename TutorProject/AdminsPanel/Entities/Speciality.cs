﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WebApplication1.Entities;

public class Speciality
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public string SubjectName { get; set; }
}