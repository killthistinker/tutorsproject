﻿namespace WebApplication1.Entities;

public class ApplicationSpecialise
{
    public long Id { get; set; }
    public string Name { get; set; }
    
    public TutorApplication TutorApplication { get; set; }
    public long TutorApplicationId { get; set; }
}