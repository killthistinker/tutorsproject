﻿namespace WebApplication1.Enums;

public enum ApplicationStatus
{
    StatusNew = 0,
    StatusPassed = 1,
    StatusNotPassed = 2
}