﻿using Responses.Contracts.Repository;
using WebApplication1.Entities;

namespace WebApplication1.Repositories.PostgresRepository.Interfaces;

public interface IApplicationRepository : IRepository<TutorApplication>
{
    public IEnumerable<TutorApplication> GetAllApplications();
    public Task<TutorApplication> GetApplicationStatusNewByEmailAsync(string email);
    public Task<bool> AnyApplication(string email);
}