using MongoDB.Driver;
using WebApplication1.Data.AdminDb;
using WebApplication1.Entities;
using WebApplication1.ViewModels;

namespace WebApplication1.Repositories.MongoRepository;

public class SubjectRepository : IEntityRepository<SubjectsViewModel>
{
    private readonly EntityContext _context;

    public SubjectRepository(EntityContext context)
    {
        _context = context;
    }
    public List<SubjectsViewModel> GetEntitiesAsync()
    {
        var subjects = _context.Subjects.ToList();
        return subjects.Select(t => new SubjectsViewModel() { Id = t.Id, Description = t.Description, Name = t.Name }).ToList();
    }
    
    public List<SubjectsViewModel> GetEntitiesByName(string name)
    {
        var subjects = _context.Subjects.Where(c => c.Name.Equals(name)).ToList();
        List<SubjectsViewModel> models = subjects.Select(t => new SubjectsViewModel() { Id = t.Id, Description = t.Description, Name = t.Name }).ToList();
        return models;
    }
    
    public void CreateEntity(SubjectsViewModel entity)
    {
        Subject model = new Subject()
            {
                Name = entity.Name,
                Description = entity.Description
            };
        _context.Subjects.Update(model);
        _context.SaveChanges();
    }

    public bool UpdateEntity(SubjectsViewModel entity)
    {
        // var updateResult = await _context.Subjects
        //     .ReplaceOneAsync(f => f.Id == entity.Id, entity);
        // return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
        return false;
    }

    public bool DeleteEntity(string id)
    {
        // FilterDefinition<Subject> filter = Builders<Subject>.Filter.Eq(p => p.Id, id);
        // var deleteResult =  _context.Subjects.DeleteOneAsync(filter);
        // return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
        return false;
    }
}