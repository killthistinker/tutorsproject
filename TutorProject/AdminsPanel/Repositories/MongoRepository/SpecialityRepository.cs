using MongoDB.Driver;
using WebApplication1.Data.AdminDb;
using WebApplication1.Entities;
using WebApplication1.ViewModels;

namespace WebApplication1.Repositories.MongoRepository;

public class SpecialityRepository : IEntityRepository<SpecialitiesViewModel>
{
    private readonly EntityContext _context;

    public SpecialityRepository(EntityContext context)
    {
        _context = context;
    }

    public List<SpecialitiesViewModel> GetEntitiesAsync()
    {
        var specialities = _context.Specialities.ToList();
        return specialities.Select(t => new SpecialitiesViewModel() { Description = t.Description, Name = t.Name, SubjectName = t.SubjectName}).ToList();
    }

    public List<SpecialitiesViewModel> GetEntitiesByName(string name)
    {
        var specialities = _context.Specialities.Where(c => c.Name == name).ToList();
        return specialities.Select(t => new SpecialitiesViewModel() { Description = t.Description, Name = t.Name, SubjectName = t.SubjectName}).ToList();
    }

    public void CreateEntity(SpecialitiesViewModel entity)
    {
        if (entity.Name != null && entity.Description != null && entity.SubjectName != null)
        {
            Speciality model = new Speciality()
            {
                Id = Guid.NewGuid().ToString(),
                Name = entity.Name,
                Description = entity.Description,
                SubjectName = entity.SubjectName
            };
            _context.Specialities.Add(model);
            _context.SaveChanges();
        }
    }

    public bool UpdateEntity(SpecialitiesViewModel entity)
    {
        return false;
    }

    public bool DeleteEntity(string id)
    {
        return false;
    }
}