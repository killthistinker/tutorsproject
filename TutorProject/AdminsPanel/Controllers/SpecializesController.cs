﻿using Microsoft.AspNetCore.Mvc;
using WebApplication1.Services.Application.Interfaces;

namespace WebApplication1.Controllers;

public class SpecializesController : Controller
{
    private readonly ISpecializeService _specializeService;

    public SpecializesController(ISpecializeService specializeService)
    {
        _specializeService = specializeService;
    }

    [HttpGet("/Specializes/GetAll")]
    public JsonResult Index(List<string> values)
    {
        var model = _specializeService.GetAllSpecialisesByValues(values);
        return Json(model);
    }
}