using FluentValidation.AspNetCore;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Repositories.MongoRepository;
using WebApplication1.Validation;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers;

public class EntitiesController : Controller
{
    private readonly IEntityRepository<SpecialitiesViewModel> _repository;
    private readonly IEntityRepository<SubjectsViewModel> _srepository;
    private readonly EntitiesVMValidator _validator;
    private readonly SpecialitiesVMValidator _specValidator;
    public EntitiesController(IEntityRepository<SpecialitiesViewModel> repository, IEntityRepository<SubjectsViewModel> srepository, EntitiesVMValidator validator, SpecialitiesVMValidator specValidator)
    {
        _repository = repository;
        _srepository = srepository;
        _validator = validator;
        _specValidator = specValidator;
    }

    [HttpGet]
    public IActionResult GetEntity()
    {
        var subjects = _srepository.GetEntitiesAsync();
        var specialities = _repository.GetEntitiesAsync();

        EntitiesViewModel model = new EntitiesViewModel()
        {
            Specialities = specialities,
            Subjects = subjects
        };
        
        return View(model);
    }
    [HttpPost]
    public async Task<IActionResult> AddSpeciality([FromBody] SpecialitiesViewModel  specialitiesViewModel)
    {
        ValidationResult result = await _specValidator.ValidateAsync(specialitiesViewModel);
        var subjects = _srepository.GetEntitiesAsync();
        var specialities = _repository.GetEntitiesAsync();
        EntitiesViewModel model = new EntitiesViewModel()
        {
            Specialities = specialities,
            Subjects = subjects
        };
        if (!result.IsValid) 
        {
            result.AddToModelState(this.ModelState);


            
            // re-render the view when validation failed.
            return View("GetEntity", model);
        }

        _repository.CreateEntity(specialitiesViewModel); //Save the person to the database, or some other logic

        TempData["notice"] = "Person successfully created";
        return RedirectToAction("GetEntity");
    }
}